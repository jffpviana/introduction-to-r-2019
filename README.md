# Introduction to R workshop 2019

This GitLab repository includes the scripts developed during the sessions of this workshop.
However, it is advisable to follow the notes and try to write and comment your own scripts.

The materials of the workshop can be found in:
https://biomedicalhub.github.io/R-intro/

You can open and edit the scripts in any text editor program. I recommend the default RStudio text editor or Notepad++.

### List of scripts available:

- Intro_R_session1.r contains the code from notes "1.Variables and Functions" and most of "2.For loops".


---------------------------------------------------------------------------------------------------

For any questions please email j.f.viana@exeter.ac.uk.

---------------------------------------------------------------------------------------------------

GitLab and GitHub are a great and free way to back-up and version control your scripts. For more information visit:

https://www.youtube.com/watch?v=fQLK8Ib_SKk

https://swcarpentry.github.io/git-novice/

