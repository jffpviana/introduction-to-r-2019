setwd("U:/data") #change directory. Make it specific to YOUR directory where you have the data
getwd() #check the current directory

read.csv(file = "inflammation-01.csv", header = FALSE) #read inflammation file, which doesn't have column names

weight_kg <- 55 #create a new variable called weight_kg with the value 55

#####explaining the difference between <- and = ####
weight_kg = 55 #different way to create a new variable called weight_kg with the value 55
55 -> weight_kg #different way to create a new variable called weight_kg with the value 55

55 = weight_kg #but this won't work but either <- and -> would work
#in other words, both 55 -> weight_kg and weight_kg <- 55 work, but only weight_kg = 55 works

2.2 * weight_kg -> weight_lb #arithmetic with the value of a variable to create a new variable

#changing the value of the first variable won't automatically change the value of the second
weight_kg <- 100 
weight_kg
weight_lb


#exercise in the slides of Session 1
mass <- 47.5
age <- 122
mass
age

mass <- mass*2.0
mass

age <- age - 20
age

read.csv("inflammation-01.csv", header = FALSE) -> dat  #read inflammation file, which doesn't have column names, and assign it to a variable called dat

class(dat) #check the class of dat. It's a data frame, a type of table or a two-dimensional array-like structure in R

dim(dat) #check the dimensions of dat

head(dat) #check the first few rows of our data frame

head(dat, 10) #check the first few rows but specify how many we want to see. Default is 6 rows.

tail(dat) #check the last few rows of our data frame. Default is 6, can change this number the same way as in head

dat[1,1] #slice element of row 1 column 1 of dat

dat[30, 20] #slice element of row 30 column 20 of dat

dat[1:4,1:10]  #slice elements of rows 1 to 4 columns 1 to 10 of dat

dat[5:10,1:10] #slice elements of rows 5 to 10 columns 1 to 10 of dat

dat[5, ] #slice elements of row 5 and all columns of dat

dat[, 20] #slice elements column 20 and all rows of dat

dat[c(3, 8, 37, 56), c(10, 14, 29)] #slice elements of rows 3, 8, 37 and 56 and columns 10, 14 and 29 of dat

animal <- c("monkey", "o", "n", "k", "e", "y") #create a vector called animal using the function combine

class(animal) #check the class of the vector animal

#exercise in the slides of Session 1
animal[1:3]
animal[4:6]


patient_1 <- dat[1,] #slice data of patient 1 from dat and save in new variable called patient_1

max(dat[1,]) #calculate the maximum inflammation value of patient 1 
min(dat[1,]) #calculate the minimum inflammation value of patient 1 
mean(as.numeric(dat[1,])) #calculate the average inflammation value of patient 1 

median(as.numeric(dat[1,])) #calculate the median inflammation value of patient 1 

sd(as.numeric(dat[1,])) #calculate the standard deviation value of patient 1 


avg_patient_inflammation <- apply(dat, 1, mean) #use function apply to calculate average inflammation values for each patient and save that in a new variable called avg_patient_inflammation


avg_day_inflammation <- apply(dat, 2, mean)  #use function apply to calculate average inflammation values for each day and save that in a new variable called avg_day_inflammation


best_practice <- c("Lets", "the", "computer", "do", "the", "work") #create a vector called best_practice using the function combine

#we want to create a function that prints each work in a different line

print_words_1 <- function(sentence) { #create a new R function called print_words_1 that takes an argument called sentence
#the function:
  print(sentence[1]) #prints the first element of sentence
  print(sentence[2]) #prints the second element of sentence
  print(sentence[3]) #prints the third element of sentence
  print(sentence[4]) #prints the fourth element of sentence
  print(sentence[5]) #prints the fifth element of sentence
  print(sentence[6]) #prints the sixth element of sentence
}

#then we use this function in our vector:
print_words_1(best_practice)
#the function above is only good for sentences with 6 words, if we used it for a vector with a different length, it would not be optimal


#we create another function which is more versatile and can be used with sentences of any length. We use a for loop for this:
print_words_2 <- function(sentence) { #create a new R function called print_words_2 that takes an argument called sentence

  for(word in sentence){ #start a for loop which goes through each element of sentence and attributes it to the new variable word
    print(word) #print the variable word
    
  }
}

#then we use this function in our vector:
print_words_2(best_practice)

#we create a for loop to gives us the length of the new vector vowels
len <- 0 #the initial length is 0
vowels <- c("a", "e", "i", "o", "u") #the vector with the vowels
for(v in vowels){ #start a for loop which goes through each element of vowels and attributes it to the new variable v
  len + 1 -> len  #in each loop we update len with len + 1, adding to the length information
}

len #check final length after the loop of over


length(vowels) #an easier way to check the length of a vector
length(best_practice)



filenames <- list.files(path = ".", pattern = "inflammation.*csv") #list the files in our current directory that contain in the file name + extension the pattern "inflammation" and "csv" with anything in between (we use .*, which is a regular expression https://stat.ethz.ch/R-manual/R-devel/library/base/html/regex.html)

filenames #check the lisrt of files

filenames<- filenames[1:3] #select the first three files

for(f in filenames){ #start a for loop which goes through each file name and attributes it to the new variable f
  dat_2 <- read.csv(f, header=FALSE) #read the current file in the loop. We attributed the file name of the current file to the variable f. 
  avg_day_inflammation <- apply(dat_2, 2, mean) #use function apply to calculate average inflammation values for each day for the current file and save that in a new variable called avg_day_inflammation

  plot(avg_day_inflammation) #plot the values of average inflammation for each day for the current file
}


